package ictgradschool.industry.lab11.ex04;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;


import java.util.ArrayList;

/**
 * Displays an animated balloon.
 */
public class ExerciseFourPanel extends JPanel implements ActionListener, KeyListener {

    private Balloon balloon;
    private Timer timer;
    private ArrayList<Balloon> balloons;
    ;

    /**
     * Creates a new ExerciseFourPanel.
     */
    public ExerciseFourPanel() {
        setBackground(Color.white);
        this.balloon = new Balloon(30, 60);
        balloons = new ArrayList<Balloon>();
        this.addKeyListener(this);
        this.timer = new Timer(20, this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (timer.isRunning()) {
            balloon.move();
            for(Balloon b: balloons){
                b.move();
            }
            requestFocusInWindow();
            repaint();
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
        timer.start();
        if (e.getKeyCode() == KeyEvent.VK_UP) {
            this.balloon.setDirection(Direction.Up);
            balloon.move();

            for(Balloon b: balloons){
                b.setDirection(Direction.Up);
                b.move();
            }
            // Sets focus to the panel itself, rather than the JButton. This way, the panel can continue to generate key
            // events even after we've clicked the button.
            requestFocusInWindow();
            repaint();
        }

        if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            this.balloon.setDirection(Direction.Down);
            balloon.move();

            for(Balloon b: balloons){
                b.setDirection(Direction.Down);
                b.move();
            }
            // Sets focus to the panel itself, rather than the JButton. This way, the panel can continue to generate key
            // events even after we've clicked the button.
            requestFocusInWindow();
            repaint();
        }

        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            this.balloon.setDirection(Direction.Left);
            balloon.move();
            for(Balloon b: balloons){
                b.setDirection(Direction.Left);
                b.move();
            }
            // Sets focus to the panel itself, rather than the JButton. This way, the panel can continue to generate key
            // events even after we've clicked the button.
            requestFocusInWindow();
            repaint();
        }

        if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            this.balloon.setDirection(Direction.Right);
            balloon.move();
            for(Balloon b: balloons){
                b.setDirection(Direction.Right);
                b.move();
            }
            // Sets focus to the panel itself, rather than the JButton. This way, the panel can continue to generate key
            // events even after we've clicked the button.
            requestFocusInWindow();
            repaint();
        }

        if (e.getKeyCode() == KeyEvent.VK_S) {
            timer.stop();

            // Sets focus to the panel itself, rather than the JButton. This way, the panel can continue to generate key
            // events even after we've clicked the button.
            requestFocusInWindow();
            repaint();
        }

        if (e.getKeyCode() == KeyEvent.VK_A) {
            int a = (int)(Math.random()*700);
            int b = (int)(Math.random()*700);
            Balloon c = new Balloon(a,b);
            balloons.add(c);
            // Sets focus to the panel itself, rather than the JButton. This way, the panel can continue to generate key
            // events even after we've clicked the button.
            requestFocusInWindow();
            repaint();
        }



    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    /**
     * Moves the balloon and calls repaint() to tell Swing we need to redraw ourselves.
     *
     * @param e
     */


    /**
     * Draws any balloons we have inside this panel.
     *
     * @param g
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        balloon.draw(g);
        for(Balloon b: balloons){
            b.draw(g);
        }

        // Sets focus outside of actionPerformed so key presses work without pressing the button
        requestFocusInWindow();
    }
}